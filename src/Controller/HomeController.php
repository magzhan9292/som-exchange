<?php

namespace App\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;

class HomeController extends AbstractController
{
    /**
     * @Route("/index", name="home")
     */
    public function index(): Response
    {
        $repository = $this->getDoctrine()->getRepository(\App\Entity\Location::class);
        $location = $repository->findAll();

        $repository = $this->getDoctrine()->getRepository(\App\Entity\Exchanger::class);

        $exchanges = $repository->getExchangers(1, 6, 0);
        $retailExchanges = $repository->getExchangers(2, 6, 0);
        $additionalExchanges = $repository->getExchangers(2, 100, 6);
	

        return $this->render('home/index.html.twig', [
            'controller_name' => 'HomeController',
            'location' => $location,
            'exchanges' => $exchanges,
			'retailExchanges' => $retailExchanges,
			'additionalExchanges' => $additionalExchanges
        ]);
    }
}
