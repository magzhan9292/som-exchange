<?php

namespace App\Entity;

use App\Repository\CurrencyRepository;
use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\Common\Collections\Collection;
use Doctrine\ORM\Mapping as ORM;

/**
 * @ORM\Entity(repositoryClass=CurrencyRepository::class)
 */
class Currency
{
    /**
     * @ORM\Id
     * @ORM\GeneratedValue
     * @ORM\Column(type="integer")
     */
    private $id;

    /**
     * @ORM\Column(type="string", length=255)
     */
    private $title;

    /**
     * @ORM\Column(type="string", length=255, nullable=true)
     */
    private $description;

    /**
     * @ORM\OneToMany(targetEntity=Exchanger::class, mappedBy="exchanger_currency_id")
	 * @ORM\OrderBy({"sort_order" = "ASC"})
     */
    private $exchanger_id;

    /**
     * @ORM\Column(type="integer")
     */
    private $sort_order;

    public function __construct()
    {
        $this->exchanger_id = new ArrayCollection();
    }

    public function getId(): ?int
    {
        return $this->id;
    }

    public function getTitle(): ?string
    {
        return $this->title;
    }

    public function setTitle(string $title): self
    {
        $this->title = $title;

        return $this;
    }

    public function getDescription(): ?string
    {
        return $this->description;
    }

    public function setDescription(?string $description): self
    {
        $this->description = $description;

        return $this;
    }

    public function getSortOrder(): ?string
    {
        return $this->sort_order;
    }

    public function setSortOrder(int $sort_order): self
    {
        $this->sort_order = $sort_order;

        return $this;
    }

    /**
     * @return Collection|Exchanger[]
     */
    public function getExchangerId(): Collection
    {
        return $this->exchanger_id;
    }

    public function addExchangerId(Exchanger $exchangerId): self
    {
        if (!$this->exchanger_id->contains($exchangerId)) {
            $this->exchanger_id[] = $exchangerId;
            $exchangerId->setExchangerCurrencyId($this);
        }

        return $this;
    }

    public function removeExchangerId(Exchanger $exchangerId): self
    {
        if ($this->exchanger_id->removeElement($exchangerId)) {
            // set the owning side to null (unless already changed)
            if ($exchangerId->getExchangerCurrencyId() === $this) {
                $exchangerId->setExchangerCurrencyId(null);
            }
        }

        return $this;
    }

    public function __toString()
    {
        // TODO: Implement __toString() method.
        return $this->title;
    }
}
