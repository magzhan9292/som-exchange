<?php

namespace App\Entity;

use App\Repository\TypeRepository;
use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\Common\Collections\Collection;
use Doctrine\ORM\Mapping as ORM;

/**
 * @ORM\Entity(repositoryClass=TypeRepository::class)
 */
class Type
{
    /**
     * @ORM\Id
     * @ORM\GeneratedValue
     * @ORM\Column(type="integer")
     */
    private $id;

    /**
     * @ORM\Column(type="string", length=255)
     */
    private $title;

    /**
     * @ORM\OneToMany(targetEntity=Exchanger::class, mappedBy="type_id")
     */
    private $exchanger_id;

    public function __construct()
    {
        $this->exchanger_id = new ArrayCollection();
    }

    public function getId(): ?int
    {
        return $this->id;
    }

    public function getTitle(): ?string
    {
        return $this->title;
    }

    public function setTitle(string $title): self
    {
        $this->title = $title;

        return $this;
    }

    /**
     * @return Collection|Exchanger[]
     */
    public function getExchangerId(): Collection
    {
        return $this->exchanger_id;
    }

    public function addExchangerId(Exchanger $exchangerId): self
    {
        if (!$this->exchanger_id->contains($exchangerId)) {
            $this->exchanger_id[] = $exchangerId;
            $exchangerId->setTypeId($this);
        }

        return $this;
    }

    public function removeExchangerId(Exchanger $exchangerId): self
    {
        if ($this->exchanger_id->removeElement($exchangerId)) {
            // set the owning side to null (unless already changed)
            if ($exchangerId->getTypeId() === $this) {
                $exchangerId->setTypeId(null);
            }
        }

        return $this;
    }

    public function __toString()
    {
        // TODO: Implement __toString() method.
        return $this->title;
    }
}
