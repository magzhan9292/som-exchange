<?php

namespace App\Entity;

use App\Repository\ExchangerRepository;
use Doctrine\ORM\Mapping as ORM;

/**
 * @ORM\Entity(repositoryClass=ExchangerRepository::class)
 */
class Exchanger
{
    /**
     * @ORM\Id
     * @ORM\GeneratedValue
     * @ORM\Column(type="integer")
     */
    private $id;

    /**
     * @ORM\ManyToOne(targetEntity=Currency::class, inversedBy="exchanger_id")
     * @ORM\JoinColumn(nullable=false)
     */
    private $exchanger_currency_id;

    /**
     * @ORM\Column(type="float")
     */
    private $buy;

    /**
     * @ORM\Column(type="float")
     */
    private $sell;

    /**
     * @ORM\ManyToOne(targetEntity=Type::class, inversedBy="exchanger_id")
     * @ORM\JoinColumn(nullable=false)
     */
    private $type_id;

    public function getId(): ?int
    {
        return $this->id;
    }

    public function getExchangerCurrencyId(): ?currency
    {
        return $this->exchanger_currency_id;
    }

    public function setExchangerCurrencyId(?currency $exchanger_currency_id): self
    {
        $this->exchanger_currency_id = $exchanger_currency_id;

        return $this;
    }

    public function getBuy(): ?string
    {
        return $this->buy;
    }

    public function setBuy(float $buy): self
    {
        $this->buy = $buy;

        return $this;
    }

    public function getSell(): ?string
    {
        return $this->sell;
    }

    public function setSell(float $sell): self
    {
        $this->sell = $sell;

        return $this;
    }

    public function getTypeId(): ?Type
    {
        return $this->type_id;
    }

    public function setTypeId(?Type $type_id): self
    {
        $this->type_id = $type_id;

        return $this;
    }
}
