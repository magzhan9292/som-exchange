<?php

namespace App\Repository;

use App\Entity\Currency;
use App\Entity\Exchanger;
use App\Entity\Type;
use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Doctrine\ORM\Query\Expr\Join;
use Doctrine\Persistence\ManagerRegistry;

/**
 * @method Exchanger|null find($id, $lockMode = null, $lockVersion = null)
 * @method Exchanger|null findOneBy(array $criteria, array $orderBy = null)
 * @method Exchanger[]    findAll()
 * @method Exchanger[]    findBy(array $criteria, array $orderBy = null, $limit = null, $offset = null)
 */
class ExchangerRepository extends ServiceEntityRepository
{
    public function __construct(ManagerRegistry $registry)
    {
        parent::__construct($registry, Exchanger::class);
    }

    /**
     * @return array
     */
    public function getExchangers($type_id = 0, int $maxResult = 1000, int $firstResult = 0): array
    {
        $result = $this->createQueryBuilder('e', 'e.id')
            ->addSelect('e.buy')
            ->addSelect('e.sell')
            ->addSelect('c.title')
            ->addSelect('c.description')
            ->andWhere('t.id = :type_id')
            ->setParameter('type_id', $type_id)
            ->leftJoin(Currency::class,'c', Join::WITH,'c.id = e.exchanger_currency_id')
            ->leftJoin(Type::class,'t', Join::WITH,'t.id = e.type_id');

        $result->orderBy('c.sort_order', 'ASC')->setMaxResults($maxResult)->setFirstResult($firstResult);
        return $result->getQuery()->getResult();
    }

    // /**
    //  * @return Exchanger[] Returns an array of Exchanger objects
    //  */
    /*
    public function findByExampleField($value)
    {
        return $this->createQueryBuilder('e')
            ->andWhere('e.exampleField = :val')
            ->setParameter('val', $value)
            ->orderBy('e.id', 'ASC')
            ->setMaxResults(10)
            ->getQuery()
            ->getResult()
        ;
    }
    */

    /*
    public function findOneBySomeField($value): ?Exchanger
    {
        return $this->createQueryBuilder('e')
            ->andWhere('e.exampleField = :val')
            ->setParameter('val', $value)
            ->getQuery()
            ->getOneOrNullResult()
        ;
    }
    */
}
