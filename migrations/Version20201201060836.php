<?php

declare(strict_types=1);

namespace DoctrineMigrations;

use Doctrine\DBAL\Schema\Schema;
use Doctrine\Migrations\AbstractMigration;

/**
 * Auto-generated Migration: Please modify to your needs!
 */
final class Version20201201060836 extends AbstractMigration
{
    public function getDescription() : string
    {
        return '';
    }

    public function up(Schema $schema) : void
    {
        // this up() migration is auto-generated, please modify it to your needs
        $this->addSql('CREATE TABLE currency (id INT AUTO_INCREMENT NOT NULL, title VARCHAR(255) NOT NULL, description VARCHAR(255) DEFAULT NULL, PRIMARY KEY(id)) DEFAULT CHARACTER SET utf8mb4 COLLATE `utf8mb4_unicode_ci` ENGINE = InnoDB');
        $this->addSql('CREATE TABLE exchanger (id INT AUTO_INCREMENT NOT NULL, exchanger_currency_id_id INT NOT NULL, type_id_id INT NOT NULL, buy DOUBLE PRECISION NOT NULL, sell DOUBLE PRECISION NOT NULL, INDEX IDX_45030DB5E788F22A (exchanger_currency_id_id), INDEX IDX_45030DB5714819A0 (type_id_id), PRIMARY KEY(id)) DEFAULT CHARACTER SET utf8mb4 COLLATE `utf8mb4_unicode_ci` ENGINE = InnoDB');
        $this->addSql('CREATE TABLE type (id INT AUTO_INCREMENT NOT NULL, title VARCHAR(255) NOT NULL, PRIMARY KEY(id)) DEFAULT CHARACTER SET utf8mb4 COLLATE `utf8mb4_unicode_ci` ENGINE = InnoDB');
        $this->addSql('ALTER TABLE exchanger ADD CONSTRAINT FK_45030DB5E788F22A FOREIGN KEY (exchanger_currency_id_id) REFERENCES currency (id)');
        $this->addSql('ALTER TABLE exchanger ADD CONSTRAINT FK_45030DB5714819A0 FOREIGN KEY (type_id_id) REFERENCES type (id)');
    }

    public function down(Schema $schema) : void
    {
        // this down() migration is auto-generated, please modify it to your needs
        $this->addSql('ALTER TABLE exchanger DROP FOREIGN KEY FK_45030DB5E788F22A');
        $this->addSql('ALTER TABLE exchanger DROP FOREIGN KEY FK_45030DB5714819A0');
        $this->addSql('DROP TABLE currency');
        $this->addSql('DROP TABLE exchanger');
        $this->addSql('DROP TABLE type');
    }
}
